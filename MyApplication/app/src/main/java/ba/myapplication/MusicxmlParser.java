package ba.myapplication;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


public class MusicxmlParser {
    private static final String ns = null;
    private int divisions;
    private int forward;
    private int backup;

    public MusicxmlParser() {
        divisions = 0;
        forward = 0;
        backup = 0;
    }

    public static class Note{
        public final boolean chord;
        public final int pitch;
        public final int voice;
        public final int duration;
        public final int staff;
        public final int backup;
        public final int forward;
        public final boolean tie;
        private Note(boolean chord, int pitch, int voice, int duration, int staff, int backup, int forward, boolean tie){
            this.chord = chord;
            this.pitch = pitch;
            this.voice = voice;
            this.duration = duration;
            this.staff = staff;
            this.backup = backup;
            this.forward = forward;
            this.tie = tie;
        }
    }

    public Part parse(InputStream in) throws XmlPullParserException, IOException {
        Part p = new Part();
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            p = readScore(parser);
        } finally {
            in.close();
        }

        for(int i = 0; i < p.part.size(); i++){
            Log.i("", Arrays.toString(p.part.get(i)));
        }
        Log.i("########################################","########################################");
        p.removeSuspensions();
        for(int i = 0; i < p.part.size(); i++){
            Log.i(Integer.toString(i), Arrays.toString(p.part.get(i)));
        }
        return p;
    }

    private Part readScore(XmlPullParser parser) throws XmlPullParserException, IOException {
        Part p = new Part();
        parser.require(XmlPullParser.START_TAG, ns, "score-partwise");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "defaults":
                    readDefaults(parser);
                    break;
                case "part":
                    p = readPart(parser);
                    break;
                default:
                    skip(parser);
                //TODO evtl readPartlist(parser);
            }
        }
        return p;
    }

    private void readDefaults(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "defaults");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "page-layout":
                    readPageLayout(parser);
                    break;
                default:
                    skip(parser);
                    //TODO ggf mehr infos nötig
            }
        }
    }

    private void readPageLayout(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, "page-layout");
        //TODO layout verarbeiten - falls nötig
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                default:
                    skip(parser);
            }
        }
    }

    private Part readPart(XmlPullParser parser) throws XmlPullParserException, IOException {
        Part p = new Part();

        parser.require(XmlPullParser.START_TAG, ns, "part");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("measure")) {
                //TODO stuff
                BuildMeasure m = readMeasure(parser);
                p.addMeasure(m.getMusic(), m.getNextPage(), m.getWidth());
            } else {
                skip(parser);
            }
        }
        //Log.i("ARRAY",Arrays.toString((boolean[])(((List)part.get(0)).get(0))));
        return p;
    }

    private BuildMeasure readMeasure(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG,ns,"measure");
        BuildMeasure buildMeasure = new BuildMeasure(divisions);
        buildMeasure.setWidth(Float.parseFloat(parser.getAttributeValue(null, "width")));
        //TODO

        while(parser.next() != XmlPullParser.END_TAG){
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "attributes":
                    int div = readAttributes(parser);
                    if(div > 0){
                        divisions = div;
                        buildMeasure.setCorrectDivisions(divisions);
                    }
                    break;
                case "print":
                    if(parser.getAttributeValue(null, "new-page")!=null && parser.getAttributeValue(null,"new-page").equals("yes")){
                        buildMeasure.setNextPage();
                    } else if(parser.getAttributeValue(null, "new-system")!=null && parser.getAttributeValue(null,"new-system").equals("yes")){
                        buildMeasure.setNextSystem();
                    }
                    skip(parser);
                    break;
                case "barline":
                    buildMeasure.setRepeat(readRepeat(parser));
                    break;
                case "note":
                    Note nNote = readNote(parser);
                    buildMeasure.addNote(nNote.chord,nNote.pitch,nNote.voice,nNote.duration,nNote.staff, nNote.tie);
                    forward = 0;
                    backup = 0;
                    break;
                case "backup":
                    backup = readDuration(parser, "backup");
                    break;
                case "forward":
                    forward = readDuration(parser, "forward");
                    break;
                default:
                    skip(parser);
            }
        }
    return buildMeasure;
    }

    private int readDuration(XmlPullParser parser, String tag) throws XmlPullParserException, IOException{
        int duration = 0;
        parser.require(XmlPullParser.START_TAG,ns,tag);
        while(parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            if(parser.getName().equals("duration")){
                duration = readIntFromTag(parser, "duration");
            } else{
                skip(parser);
            }
        }

        return duration;
    }

    private int readRepeat(XmlPullParser parser) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG,ns,"barline");
        int repeat = 0;
        while(parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            if(parser.getName().equals("repeat")){
                if(parser.getAttributeValue(null, "direction").equals("forward")){
                    repeat = -1;
                } else if(parser.getAttributeValue(null, "direction").equals("backward")){
                    repeat = 1;
                }
            }
            skip(parser);
        }
        return repeat;
    }

    private int readAttributes(XmlPullParser parser) throws XmlPullParserException, IOException{
        int divisions = 0;
        int time = 0;
        parser.require(XmlPullParser.START_TAG,ns,"attributes");
        while(parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "divisions":
                    divisions = readIntFromTag(parser, "divisions");
                    break;
                case "time":
                    time = readTime(parser);
                    break;
                default:
                    skip(parser);
            }
        }
        return divisions*time;
    }

    private int readTime(XmlPullParser parser) throws XmlPullParserException, IOException{
        int beats = 1;
        int type = 1;
        parser.require(XmlPullParser.START_TAG,ns,"time");
        while(parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "beats":
                    beats = readIntFromTag(parser, "beats");
                    break;
                case "beat-type":
                    type = readIntFromTag(parser,"beat-type");
                    break;
                default:
                    skip(parser);
            }
        }
        return (4 / type * beats);
    }

    private Note readNote(XmlPullParser parser) throws XmlPullParserException, IOException{
        int duration = 0;
        int pitch = -1;
        int voice = 0;
        int staff = 0;
        boolean tie = false;
        boolean chord = false;

        parser.require(XmlPullParser.START_TAG,ns,"note");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "chord":
                    chord = true;
                    skip(parser);
                    break;
                case "rest":
                    pitch = -1;
                    skip(parser);
                    break;
                case "pitch":
                    pitch = readPitch(parser);
                    break;
                case "voice":
                    voice = readIntFromTag(parser, "voice");
                    break;
                case "duration":
                    duration = readIntFromTag(parser,"duration");
                    break;
                case "staff":
                    staff = readIntFromTag(parser,"staff");
                    break;
                case "tie":
                    tie = parser.getAttributeValue(null, "type").equals("stop");
                    skip(parser);
                    break;
                default:
                    skip(parser);
            }
        }
        return new Note(chord, pitch, voice, duration, staff, backup, forward, tie);
    }

    private int readPitch(XmlPullParser parser) throws XmlPullParserException, IOException{
        int step = 0;
        int octave = 0;
        int alter = 0;
        parser.require(XmlPullParser.START_TAG,ns,"pitch");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            switch (tag){
                case "step":
                    step = readStep(parser);
                    break;
                case "octave":
                    octave = readIntFromTag(parser, "octave");
                    break;
                case "alter":
                    alter = readIntFromTag(parser, "alter");
                    break;
                default:
                    skip(parser);
            }
        }
        return step + (octave-1)*12 + alter;
    }

    private int readStep(XmlPullParser parser) throws XmlPullParserException, IOException{
        int step;
        parser.require(XmlPullParser.START_TAG, ns, "step");
        String content = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "step");
        switch (content){
            case "C":
                step = 1;
                break;
            case "D":
                step = 3;
                break;
            case "E":
                step = 5;
                break;
            case "F":
                step = 6;
                break;
            case "G":
                step = 8;
                break;
            case "A":
                step = 10;
                break;
            case "B":
                step = 12;
                break;
            default:
                step = 0;
        }
        return step;
    }

    private int readIntFromTag(XmlPullParser parser, String tag) throws XmlPullParserException, IOException{
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String content = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return Integer.parseInt(content);
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}