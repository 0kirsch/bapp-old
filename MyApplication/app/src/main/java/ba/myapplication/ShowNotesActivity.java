package ba.myapplication;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ShowNotesActivity extends ActionBarActivity {

    private Spinner spinner;
    private File songMidi = null;
    private File songPdf = null;
    private File songXml = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notes);
        Intent intent = getIntent();
        String pathOfSelectedSong = intent.getStringExtra("path") + "/" + intent.getStringExtra("file") + "/" + intent.getStringExtra("file");
        songMidi = new File(pathOfSelectedSong + ".mid");
        songPdf = new File(pathOfSelectedSong + ".pdf");
        songXml = new File(pathOfSelectedSong + ".xml");
        MusicxmlParser mxmlp = new MusicxmlParser();
        InputStream is = null;
        try{
            is = new FileInputStream(pathOfSelectedSong + ".xml");
            Log.i("###################################","#############################");
            Part part = mxmlp.parse(is);
        }
        catch(XmlPullParserException|IOException e){
            e.printStackTrace();
        }
        goToWebView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // click on the "Browser" button in view a
    public void goToWebView() {
        WebView mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAllowFileAccessFromFileURLs(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        mWebView.loadUrl("file:///android_asset/score-viewer.html");
    }

    public void firstPage(View view){
        turnPage("page_1st_btn");
    }
    public void prevPage(View view){
        turnPage("page_prev_btn");
    }
    public void nextPage(View view){
        turnPage("page_next_btn");
    }

    public void addPagesToSpinner(){
        //do stuff

    }
    private void turnPage(String button){
        WebView mWebView = (WebView) findViewById(R.id.webView);
        mWebView.loadUrl("javascript:document.getElementById(\"" + button + "\").click();");

    }

    // override default behaviour of the browser
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url){
            view.loadUrl("javascript:(function(){" +
                    "document.getElementById('toolbar').style.display='none';})()");
        }
    }
}
