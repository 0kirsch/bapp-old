package ba.myapplication;


import java.util.ArrayList;
import java.util.List;

public class Part {
    List<Measure> measures;
    List<boolean[]> part;

    public static class Measure{
        public final int position;
        public final int duration;
        public final boolean lastMeasure;
        public final float width;

        private Measure(boolean lastMeasure, int position, int duration, float width){
            this.position = position;
            this.lastMeasure = lastMeasure;
            this.duration = duration;
            this.width = width;

        }
    }

    Part(){
        measures = new ArrayList<>();
        part = new ArrayList<>();
    }

    public void addMeasure(List<boolean[]> notes, boolean lastMeasure, float width){
        int position;
        if(measures.size() == 0){
            position = 0;
        } else {
            Measure oldMeasure = measures.get(measures.size()-1);
            position = oldMeasure.position + oldMeasure.duration;
        }
        Measure m = new Measure(lastMeasure, position, part.size(), width);
        measures.add(m);
        part.addAll(notes);
    }

    public void removeSuspensions(){
        for(int i = 0; i < part.size(); i++ ){
            if(!part.get(i)[0]){
                part.remove(i);
                i -= 1;
            }
        }
    }
}
