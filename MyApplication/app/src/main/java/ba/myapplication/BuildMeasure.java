package ba.myapplication;


import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BuildMeasure {
    private List<boolean[]> beats;
    private int oldEnd;
    private int oldVoice;
    private int oldStaff;
    private boolean nextSystem;
    private boolean nextPage;
    private int repeat;
    private float width;

    BuildMeasure(int beatcount){
        beats = new ArrayList<>();
        fillMeasure(beatcount);
        repeat = 0;
        nextPage = false;
        nextSystem = false;
        oldStaff = 0;
        oldVoice = 0;
        oldEnd = 0;
        width = 0.0f;
    }

    public void setCorrectDivisions(int beatcount){
        beats = new ArrayList<>();
        fillMeasure(beatcount);
    }

    private void fillMeasure(int beatcount){
        for(int i = 0; i < beatcount; i++){
            boolean[] array = new boolean[98];
            Arrays.fill(array, false);
            beats.add(array);
        }
    }

    public void addNote(boolean chord, int pitch, int voice, int duration, int staff, boolean tie) {
        int start = 0;

        //check voice&staff
        if(voice != oldVoice || staff != oldStaff){
            start = 0;
            oldEnd = 0;
            oldVoice = voice;
            oldStaff = staff;
        } else{
            start = oldEnd;
        }

        // Pause -> alles bleibt wie es ist
        if (pitch < 0) {
            boolean[] beat = beats.get(start);
            beat[0] = true;
            beats.set(start, beat);
            oldEnd += duration;
        }
        // Ton einfügen
        else {
            if (chord) {
                start = oldEnd - duration;
            } else {
                start = oldEnd;

                oldEnd += duration;
            }
            for (int i = 0; i < duration; i++) {
                boolean[] beat = beats.get(start + i);
                beat[pitch] = true;
                if(i == 0 && !tie){
                    beat[0] = true;
                }
                beats.set(start + i, beat);
            }
        }
    }

    public void setNextSystem(){
        nextSystem = true;
    }

    public void setNextPage(){
        nextPage = true;
    }

    public void setRepeat(int r){
        repeat = r;
    }

    public void setWidth(float w) {
        width = w;
    }

    public boolean getNextSystem(){
        return nextSystem;
    }

    public boolean getNextPage(){
        return nextPage;
    }

    public int getRepeat(){
        return repeat;
    }

    public List<boolean[]> getMusic() {
        return beats;
    }

    public float getWidth(){
        return width;
    }

}